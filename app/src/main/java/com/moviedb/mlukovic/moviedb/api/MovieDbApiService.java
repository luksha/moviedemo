package com.moviedb.mlukovic.moviedb.api;

import com.moviedb.mlukovic.moviedb.ui.movie_details.model.MovieDetails;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieListResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface MovieDbApiService {

    @GET("movie/popular")
    Single<MovieListResponse> getTopMovies();

    @GET("movie/{id}")
    Single<MovieDetails> getMovie(@Path("id") String id);
}
