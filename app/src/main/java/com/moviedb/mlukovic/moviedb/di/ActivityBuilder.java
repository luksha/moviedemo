package com.moviedb.mlukovic.moviedb.di;

import com.moviedb.mlukovic.moviedb.ui.movie_details.MovieDetailsActivity;
import com.moviedb.mlukovic.moviedb.ui.movie_details.di.MovieDetailsModule;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieListActivity;
import com.moviedb.mlukovic.moviedb.ui.movie_list.di.MovieListModule;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class ActivityBuilder {

    @ContributesAndroidInjector(modules = MovieListModule.class)
    abstract MovieListActivity bindMovieListActivity();

    @ContributesAndroidInjector(modules = MovieDetailsModule.class)
    abstract MovieDetailsActivity bindMovieDetailsActivity();

}
