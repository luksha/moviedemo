package com.moviedb.mlukovic.moviedb.ui.base;

import android.arch.lifecycle.LifecycleOwner;

public interface BaseContract {

    interface View extends LifecycleOwner {

    }

    interface Presenter<T extends View> {

        void onViewStarted();

        void onViewStopped();
    }
}
