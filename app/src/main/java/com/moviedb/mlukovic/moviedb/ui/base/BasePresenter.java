package com.moviedb.mlukovic.moviedb.ui.base;

import android.arch.lifecycle.LifecycleObserver;
import android.arch.lifecycle.OnLifecycleEvent;

import static android.arch.lifecycle.Lifecycle.Event.ON_START;
import static android.arch.lifecycle.Lifecycle.Event.ON_STOP;

public abstract class BasePresenter<T extends BaseContract.View> implements BaseContract.Presenter<T>,
        LifecycleObserver {
    private T view;

    public BasePresenter(T view) {
        this.view = view;
        view.getLifecycle().addObserver(this);
    }

    protected T getView() {
        return view;
    }

    @OnLifecycleEvent(ON_START)
    void start() {
        onViewStarted();
    }

    @OnLifecycleEvent(ON_STOP)
    void stop() {
        onViewStopped();
    }
}
