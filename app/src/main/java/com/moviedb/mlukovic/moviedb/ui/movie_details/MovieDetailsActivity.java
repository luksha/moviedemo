package com.moviedb.mlukovic.moviedb.ui.movie_details;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.moviedb.mlukovic.moviedb.R;
import com.moviedb.mlukovic.moviedb.databinding.ActivityMovieDetailsBinding;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MovieDetailsActivity extends AppCompatActivity implements MovieDetailsContract.View {

    public static final String BUNDLE_MOVIE_ID = "movie_id";

    @Inject
    MovieDetailsContract.Presenter presenter;
    private ActivityMovieDetailsBinding viewBinding;

    public static Intent getIntent(Context context, String movieId) {
        Intent intent = new Intent(context, MovieDetailsActivity.class);
        intent.putExtra(BUNDLE_MOVIE_ID, movieId);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_details);
    }

    @Override
    protected void onStart() {
        super.onStart();
        String movieId = getIntent().getStringExtra(BUNDLE_MOVIE_ID);
        presenter.loadData(movieId);
    }

    @Override
    public void showDescription(String text) {
        viewBinding.descriptionText.setText(text);
        viewBinding.descriptionText.setVisibility(View.VISIBLE);
        viewBinding.errorMessage.setVisibility(View.GONE);
        viewBinding.loadingSpinner.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        viewBinding.errorMessage.setVisibility(View.VISIBLE);
        viewBinding.loadingSpinner.setVisibility(View.GONE);
        viewBinding.descriptionText.setVisibility(View.GONE);
    }

    @Override
    public void showLoader(boolean visible) {
        viewBinding.loadingSpinner.setVisibility(visible ? View.VISIBLE : View.GONE);
        viewBinding.errorMessage.setVisibility(View.GONE);
        viewBinding.descriptionText.setVisibility(View.GONE);
    }
}