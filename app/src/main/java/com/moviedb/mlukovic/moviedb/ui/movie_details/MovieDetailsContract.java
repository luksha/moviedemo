package com.moviedb.mlukovic.moviedb.ui.movie_details;

import com.moviedb.mlukovic.moviedb.ui.base.BaseContract;

public interface MovieDetailsContract {

    interface View extends BaseContract.View {

        void showDescription(String text);

        void showError();

        void showLoader(boolean visible);
    }

    interface Presenter {

        void loadData(String movieId);
    }
}
