package com.moviedb.mlukovic.moviedb.ui.movie_details;

import com.moviedb.mlukovic.moviedb.ui.base.BasePresenter;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieRepository;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

public class MovieDetailsPresenter extends BasePresenter<MovieDetailsContract.View> implements MovieDetailsContract.Presenter {


    private final MovieRepository repository;
    private final Scheduler mainThreadScheduler;
    private final Scheduler ioScheduler;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public MovieDetailsPresenter(MovieDetailsContract.View view, MovieRepository repository,
                                 Scheduler mainThreadScheduler, Scheduler ioScheduler) {
        super(view);
        this.repository = repository;
        this.mainThreadScheduler = mainThreadScheduler;
        this.ioScheduler = ioScheduler;
    }

    @Override
    public void loadData(String movieId) {
        getView().showLoader(true);
        compositeDisposable.add(repository.getMovie(movieId)
                .subscribeOn(ioScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe(movieDetails -> {
                            getView().showLoader(false);
                            getView().showDescription(movieDetails.getOverview());
                        },
                        error -> {
                            getView().showLoader(false);
                            getView().showError();
                        }));
    }

    @Override
    public void onViewStarted() {
        // do nothing
    }

    @Override
    public void onViewStopped() {
        compositeDisposable.clear();
    }
}
