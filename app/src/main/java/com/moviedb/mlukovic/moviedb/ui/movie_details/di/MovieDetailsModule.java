package com.moviedb.mlukovic.moviedb.ui.movie_details.di;

import com.moviedb.mlukovic.moviedb.api.MovieDbApiService;
import com.moviedb.mlukovic.moviedb.di.AppModule;
import com.moviedb.mlukovic.moviedb.ui.movie_details.MovieDetailsActivity;
import com.moviedb.mlukovic.moviedb.ui.movie_details.MovieDetailsContract;
import com.moviedb.mlukovic.moviedb.ui.movie_details.MovieDetailsPresenter;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieRepository;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class MovieDetailsModule {

    @Provides
    MovieDetailsContract.View provideMovieListView(MovieDetailsActivity movieListActivity) {
        return movieListActivity;
    }

    @Provides
    MovieDetailsContract.Presenter provideMovieListPresenter(MovieDetailsContract.View view,
                                                             MovieRepository movieRepository,
                                                             @Named(AppModule.MAIN_SCHEDULER) Scheduler mainThreadScheduler,
                                                             @Named(AppModule.IO_SCHEDULER) Scheduler ioScheduler) {
        return new MovieDetailsPresenter(view, movieRepository, mainThreadScheduler, ioScheduler);
    }

    @Provides
    MovieRepository provideMovieRepository(MovieDbApiService movieDbApiService) {
        return new MovieRepository(movieDbApiService);
    }
}
