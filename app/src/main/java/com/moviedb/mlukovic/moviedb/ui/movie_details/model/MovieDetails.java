package com.moviedb.mlukovic.moviedb.ui.movie_details.model;

import com.google.gson.annotations.SerializedName;

public class MovieDetails {

    @SerializedName("overview")
    private String overview;

    @SerializedName("title")
    private String title;

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
