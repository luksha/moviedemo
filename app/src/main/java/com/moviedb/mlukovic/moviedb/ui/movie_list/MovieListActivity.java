package com.moviedb.mlukovic.moviedb.ui.movie_list;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.moviedb.mlukovic.moviedb.R;
import com.moviedb.mlukovic.moviedb.databinding.ActivityMovieListBinding;
import com.moviedb.mlukovic.moviedb.ui.movie_details.MovieDetailsActivity;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

import java.util.List;

import javax.inject.Inject;

import dagger.android.AndroidInjection;

public class MovieListActivity extends AppCompatActivity implements MovieListContract.View, MovieListAdapter.OnMovieClickListener {

    @Inject
    MovieListContract.Presenter moviePresenter;
    ActivityMovieListBinding viewBinding;
    private MovieListAdapter movieListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        AndroidInjection.inject(this);
        super.onCreate(savedInstanceState);
        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_movie_list);
        initMovieList();
    }

    private void initMovieList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        movieListAdapter = new MovieListAdapter(null, this);
        viewBinding.movieList.setLayoutManager(layoutManager);
        viewBinding.movieList.setAdapter(movieListAdapter);
    }

    @Override
    public void showMovieList(List<MovieItem> movieItems) {
        movieListAdapter.setMovieList(movieItems);
    }

    @Override
    public void showLoader(boolean visible) {
        viewBinding.loadingSpinner.setVisibility(visible ? View.VISIBLE : View.GONE);
        viewBinding.movieList.setVisibility(visible ? View.GONE : View.VISIBLE);
        viewBinding.errorMessage.setVisibility(View.GONE);
    }

    @Override
    public void showError() {
        viewBinding.errorMessage.setVisibility(View.VISIBLE);
        viewBinding.loadingSpinner.setVisibility(View.GONE);
        viewBinding.movieList.setVisibility(View.GONE);
    }

    @Override
    public void onMovieClicked(MovieItem movieItem) {
        moviePresenter.onMovieSelected(movieItem);
    }

    @Override
    public void navigateToMovieDetails(String movieId) {
        Intent intent = MovieDetailsActivity.getIntent(this, movieId);
        startActivity(intent);
    }

    @Override
    public void showErrorToNavigate() {
        Toast.makeText(this, R.string.error_can_not_open_movie_details, Toast.LENGTH_SHORT).show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_filter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_sort:
                moviePresenter.sortMovieList();
                break;
            default:
                break;
        }
        return true;
    }
}