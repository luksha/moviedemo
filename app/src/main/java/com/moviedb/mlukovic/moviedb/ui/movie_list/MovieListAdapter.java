package com.moviedb.mlukovic.moviedb.ui.movie_list;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.moviedb.mlukovic.moviedb.databinding.MovieListItemBinding;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

import java.util.List;

public class MovieListAdapter extends RecyclerView.Adapter<MovieViewHolder> {

    private List<MovieItem> movieList;
    private OnMovieClickListener movieClickListener;

    public MovieListAdapter(List<MovieItem> movieList, OnMovieClickListener movieClickListener) {
        this.movieList = movieList;
        this.movieClickListener = movieClickListener;
    }

    @NonNull
    @Override
    public MovieViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());
        MovieListItemBinding movieListItemBinding = MovieListItemBinding.inflate(inflater, viewGroup, false);
        return new MovieViewHolder(movieListItemBinding, movieClickListener);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieViewHolder holder, int position) {
        holder.bind(movieList.get(position));
    }

    @Override
    public int getItemCount() {
        return movieList == null ? 0 : movieList.size();
    }

    public void setMovieList(List<MovieItem> movieItems) {
        this.movieList = movieItems;
        notifyDataSetChanged();
    }

    public interface OnMovieClickListener {
        void onMovieClicked(MovieItem movieItem);
    }
}