package com.moviedb.mlukovic.moviedb.ui.movie_list;

import com.moviedb.mlukovic.moviedb.ui.base.BaseContract;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

import java.util.List;

public interface MovieListContract {

    interface View extends BaseContract.View {

        void showMovieList(List<MovieItem> movieItems);

        void showLoader(boolean visible);

        void showError();

        void navigateToMovieDetails(String movieId);

        void showErrorToNavigate();
    }

    interface Presenter {

        void onMovieSelected(MovieItem movieItem);

        void sortMovieList();
    }
}
