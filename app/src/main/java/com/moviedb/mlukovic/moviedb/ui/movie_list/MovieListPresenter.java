package com.moviedb.mlukovic.moviedb.ui.movie_list;

import com.moviedb.mlukovic.moviedb.ui.base.BasePresenter;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.disposables.CompositeDisposable;

public class MovieListPresenter extends BasePresenter<MovieListContract.View> implements MovieListContract.Presenter {

    private MovieRepository movieRepository;
    private Scheduler mainThreadScheduler;
    private Scheduler ioScheduler;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public MovieListPresenter(MovieListContract.View view, MovieRepository movieRepository,
                              Scheduler mainThreadScheduler, Scheduler ioScheduler) {
        super(view);
        this.movieRepository = movieRepository;
        this.mainThreadScheduler = mainThreadScheduler;
        this.ioScheduler = ioScheduler;
    }

    @Override
    public void onViewStarted() {
        getView().showLoader(true);
        compositeDisposable.add(movieRepository.getTopMovies()
                .subscribeOn(ioScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe(
                        movieItems -> {
                            getView().showLoader(false);
                            getView().showMovieList(movieItems);
                        },
                        error -> {
                            getView().showLoader(false);
                            getView().showError();
                        }));
    }

    @Override
    public void onViewStopped() {
        compositeDisposable.clear();
    }

    @Override
    public void onMovieSelected(MovieItem movieItem) {
        if (movieItem != null) {
            getView().navigateToMovieDetails(movieItem.getId());
        } else {
            getView().showErrorToNavigate();
        }
    }

    @Override
    public void sortMovieList() {
        getView().showLoader(true);
        compositeDisposable.add(movieRepository.getTopMoviesSortByDate()
                .subscribeOn(ioScheduler)
                .observeOn(mainThreadScheduler)
                .subscribe(
                        movieItems -> {
                            getView().showLoader(false);
                            getView().showMovieList(movieItems);
                        },
                        error -> {
                            getView().showLoader(false);
                            getView().showError();
                        }));
    }
}
