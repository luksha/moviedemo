package com.moviedb.mlukovic.moviedb.ui.movie_list;

import com.moviedb.mlukovic.moviedb.api.MovieDbApiService;
import com.moviedb.mlukovic.moviedb.ui.movie_details.model.MovieDetails;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieListResponse;

import java.util.List;

import io.reactivex.Single;

public class MovieRepository {

    private MovieDbApiService apiService;
    private List<MovieItem> movieItemListCache;
    private MovieDetails movieDetailsCache;

    public MovieRepository(MovieDbApiService apiService) {
        this.apiService = apiService;
    }

    public Single<List<MovieItem>> getTopMovies() {
        if (movieItemListCache != null) {
            return Single.just(movieItemListCache);
        } else {
            return apiService.getTopMovies()
                    .map(MovieListResponse::getMovieList)
                    .doOnSuccess(movieItems -> movieItemListCache = movieItems);
        }
    }

    public Single<List<MovieItem>> getTopMoviesSortByDate() {
        return getTopMovies()
                .toObservable()
                .flatMapIterable(list -> list)
                .toSortedList(MovieItem::compareTo);
    }

    public Single<MovieDetails> getMovie(String id) {
        if (movieDetailsCache != null) {
            return Single.just(movieDetailsCache);
        } else {
            return apiService.getMovie(id).doOnSuccess(movieDetails -> movieDetailsCache = movieDetails);
        }
    }
}