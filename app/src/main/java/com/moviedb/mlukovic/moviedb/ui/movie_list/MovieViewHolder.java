package com.moviedb.mlukovic.moviedb.ui.movie_list;

import android.support.v7.widget.RecyclerView;

import com.moviedb.mlukovic.moviedb.databinding.MovieListItemBinding;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

class MovieViewHolder extends RecyclerView.ViewHolder {

    private MovieListItemBinding movieListItemBinding;
    private MovieListAdapter.OnMovieClickListener movieClickListener;

    public MovieViewHolder(MovieListItemBinding viewBinding, MovieListAdapter.OnMovieClickListener clickListener) {
        super(viewBinding.getRoot());
        movieListItemBinding = viewBinding;
        this.movieClickListener = clickListener;
    }

    void bind(MovieItem movieItem) {
        movieListItemBinding.setMovie(movieItem);
        movieListItemBinding.getRoot().setOnClickListener(view -> movieClickListener.onMovieClicked(movieItem));
    }
}
