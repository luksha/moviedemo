package com.moviedb.mlukovic.moviedb.ui.movie_list.di;

import com.moviedb.mlukovic.moviedb.api.MovieDbApiService;
import com.moviedb.mlukovic.moviedb.di.AppModule;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieListActivity;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieListContract;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieListPresenter;
import com.moviedb.mlukovic.moviedb.ui.movie_list.MovieRepository;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import io.reactivex.Scheduler;

@Module
public class MovieListModule {

    @Provides
    MovieListContract.View provideMovieListView(MovieListActivity movieListActivity) {
        return movieListActivity;
    }

    @Provides
    MovieListContract.Presenter provideMovieListPresenter(MovieListContract.View view,
                                                          MovieRepository movieRepository,
                                                          @Named(AppModule.MAIN_SCHEDULER) Scheduler mainThreadScheduler,
                                                          @Named(AppModule.IO_SCHEDULER) Scheduler ioScheduler) {
        return new MovieListPresenter(view, movieRepository, mainThreadScheduler, ioScheduler);
    }

    @Provides
    MovieRepository provideMovieRepository(MovieDbApiService movieDbApiService) {
        return new MovieRepository(movieDbApiService);
    }
}
