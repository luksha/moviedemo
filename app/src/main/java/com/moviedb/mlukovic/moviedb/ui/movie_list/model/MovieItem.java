package com.moviedb.mlukovic.moviedb.ui.movie_list.model;

import android.support.annotation.NonNull;

import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class MovieItem implements Comparable<MovieItem> {

    @SerializedName("id")
    private String id;

    @SerializedName("title")
    private String title;

    @SerializedName("release_date")
    private String releaseDate;

    @SerializedName("overview")
    private String overview;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    @Override
    public int compareTo(@NonNull MovieItem movieItem) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date1;
        Date date2;
        try {
            date1 = format.parse(movieItem.getReleaseDate());
            date2 = format.parse(this.releaseDate);
        } catch (ParseException e) {
            return 0;
        }
        return date1.compareTo(date2);
    }
}
