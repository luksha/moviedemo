package com.moviedb.mlukovic.moviedb.ui.movie_list.model;

import com.google.gson.annotations.SerializedName;
import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

import java.util.List;

public class MovieListResponse {

    @SerializedName("name")
    private int page;

    @SerializedName("total_pages")
    private int totalPages;

    @SerializedName("results")
    private List<MovieItem> movieList;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public List<MovieItem> getMovieList() {
        return movieList;
    }

    public void setMovieList(List<MovieItem> movieList) {
        this.movieList = movieList;
    }
}
