package com.moviedb.mlukovic.moviedb.ui.movie_list;

import android.arch.lifecycle.Lifecycle;

import com.moviedb.mlukovic.moviedb.ui.movie_list.model.MovieItem;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Scheduler;
import io.reactivex.Single;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.schedulers.TestScheduler;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MovieListPresenterTest {

    @Mock
    private MovieListContract.View view;
    @Mock
    private MovieRepository movieRepository;
    private Scheduler trampolineScheduler = Schedulers.trampoline();
    private TestScheduler testScheduler = new TestScheduler();
    private MovieListPresenter presenter;

    @Before
    public void setUp() throws Exception {
        when(view.getLifecycle()).thenReturn(mock(Lifecycle.class));
        presenter = new MovieListPresenter(view, movieRepository, testScheduler, trampolineScheduler);
    }

    @Test
    public void onViewStarted_OnSuccess_ShowList() throws Exception {
        //Given
        List<MovieItem> movieItems = new ArrayList<>();
        movieItems.add(new MovieItem());

        when(movieRepository.getTopMovies()).thenReturn(Single.just(movieItems));

        //When
        presenter.onViewStarted();
        testScheduler.triggerActions();

        //Then
        verify(view).showLoader(true);
        verify(view).showMovieList(movieItems);
        verify(view).showLoader(false);
    }

    @Test
    public void onViewStarted_OnError_ShowError() throws Exception {
        //Given
        when(movieRepository.getTopMovies()).thenReturn(Single.error(mock(Throwable.class)));

        //When
        presenter.onViewStarted();
        testScheduler.triggerActions();

        //Then
        verify(view).showLoader(true);
        verify(view).showError();
        verify(view).showLoader(false);
    }

    @Test
    public void onViewStarted_ThenStopped_DoNotCallView() throws Exception {
        //Given
        when(movieRepository.getTopMovies()).thenReturn(Single.error(mock(Throwable.class)));

        //When
        presenter.onViewStarted();
        presenter.onViewStopped();
        testScheduler.triggerActions();

        //Then
        verify(view).showLoader(true);
        verify(view, never()).showError();
        verify(view, never()).showLoader(false);
    }

    @Test
    public void onMovieSelected_NotNull_NavigateToMovieDetails() throws Exception {
        //Given
        String testId = "testId";
        MovieItem movieItem = new MovieItem();
        movieItem.setId(testId);

        //When
        presenter.onMovieSelected(movieItem);

        //Then
        verify(view).navigateToMovieDetails(testId);
    }

    @Test
    public void onMovieSelected_MovieIsNull_showError() throws Exception {
        //Given
        MovieItem movieItem = null;

        //When
        presenter.onMovieSelected(movieItem);

        //Then
        verify(view).showErrorToNavigate();
    }
}